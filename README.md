# Stud.IP File Name Cleaner

> _Python 3_ script for cleaning prefixed file names and archive file lists of **Stud.IP** folder downloads

## What does the script do?

1. Remove files named `archive_filelist.csv`
2. Remove numerical prefixes like `[0]_my-file.pdf` by renaming the files

## Requirements

* Have a _Python ≥ 3.8_ installation
* Optionally install `Fire`, `tqdm` and `rich` using `pip install .` or `poetry install`

## Usage

* Optionally activate virtual environment when using `poetry` by using `poetry shell`

### Without additional dependencies

* Clean current working directory recursively via `python clean_studip_files.py` and confirmation by typing `yes`

### With `Fire` installed

* Access commands help via `python clean_studip_files.py --help`
* Remove archive file list via `python clean_studip_files.py remove_filelist <Target Directory> [recursive=False]`
* Rename prefixed files via `python clean_studip_files.py remove_prefixed <Target Directory> [recursive=False]`
* Clean directory (both previous commands) via `python clean_studip_files.py clean <Target Directory> [recursive=False]`
* Clean current working directory directory (both previous commands) via `python clean_studip_files.py clean_cwd [recursive=False]`
