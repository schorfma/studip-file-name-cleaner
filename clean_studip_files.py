from pathlib import Path
from typing import Iterable, List, Text, Union


PROGRESS_ITERABLE: Iterable

try:
    from tqdm import tqdm
    PROGRESS_ITERABLE = tqdm
except ImportError:
    PROGRESS_ITERABLE = lambda iterable, **kwargs: iterable

RICH_PRINT_ACTIVE: bool

try:
    from rich import print
    from rich.table import Table as RichTable, Column as RichColumn
    from rich.live import Live as RichLive
    add_table = lambda *items, **kwargs: RichTable(
        *[
            RichColumn(
                item,
                overflow="fold"
            )
            for item in items
        ],
        **kwargs
    )
    add_row = lambda table, *items, **kwargs: table.add_row(
        *items,
        **kwargs
    )
    RICH_PRINT_ACTIVE = True
except ImportError:
    import contextlib
    RichLive = contextlib.suppress
    RICH_PRINT_ACTIVE = False
    add_table = lambda *items, **kwargs: "\t".join(items)
    add_row = lambda table, *items, **kwargs: add_table(*items, **kwargs)


ARCHIVE_FILE_LIST_NAME = "archive_filelist.csv"

PREFIXED_FILES_GLOB_PATTERN = r"[[][0-9]*[]]_*.*"

PREFIXED_FILES_SPLIT_PATTERN = "]_"




def normalize_directory(
        directory: Union[Text, Path]
) -> Path:
    """Ensures provided directory is a Path object."""

    target_directory: Path

    if not isinstance(directory, Path):
        target_directory = Path(directory)
    else:
        target_directory = directory

    if target_directory.exists():
        print("Using directory", target_directory)
    else:
        raise ValueError(
            f"Directory does not exist: {target_directory}"
        )

    return target_directory


def remove_archive_file_lists(
        directory: Union[Text, Path],
        recursive: bool = True
):
    """Removes 'archive_filelist.csv' files in the directory - recursively
    by default.
    """

    try:
        target_directory: Path = normalize_directory(directory)
    except ValueError as value_error:
        print(
            value_error
        )
        return

    target_files: List[Path]

    if not recursive:
        target_file = target_directory / ARCHIVE_FILE_LIST_NAME
        target_files = [target_file]
    else:
        target_files = list(
            target_directory.rglob(ARCHIVE_FILE_LIST_NAME)
        )

    table = add_table(
        "INDEX NUMBER", "REMOVAL TARGET FILE",
        expand=True,
        show_lines=True,
        title=f"Removing '{ARCHIVE_FILE_LIST_NAME}' files"
    )

    with RichLive(table):
        for target_file_index, target_file in PROGRESS_ITERABLE(
            list(
                enumerate(target_files, start=1)
            ),
            desc=f"Removal of '{ARCHIVE_FILE_LIST_NAME}' files"
        ):

            add_row(
                table,
                str(target_file_index),
                target_file.as_posix()
            )

            target_file.unlink()

    print()


def rename_prefixed_files(
        directory: Union[Text, Path],
        recursive: bool = True
):
    """Rename prefixed files in the directory - recursively by default."""

    target_directory: Path = normalize_directory(directory)

    target_files: List[Path]

    if not recursive:
        target_files = list(
            target_directory.glob(PREFIXED_FILES_GLOB_PATTERN)
        )
    else:
        target_files = list(
            target_directory.rglob(PREFIXED_FILES_GLOB_PATTERN)
        )

    table = add_table(
        "INDEX NUMBER", "TARGET FILE",
        "=>", "CHANGED TARGET FILE",
        "SUCCESS STATUS",
        expand=True,
        show_lines=True,
        title="Renaming prefixed files"
    )

    with RichLive(table):
        for target_file_index, target_file in PROGRESS_ITERABLE(
            list(
                enumerate(target_files, start=1)
            ),
            desc="Renaming prefixed files"
        ):
            changed_target_file = target_file.with_name(
                target_file.name.split(
                    PREFIXED_FILES_SPLIT_PATTERN,
                    maxsplit=1
                )[-1]
            )

            rename_success: bool

            try:
                target_file.rename(
                    changed_target_file
                )
                rename_success = True
            except Exception as exception:
                print("Not renamed, because of exception", exception)
                rename_success = False

            add_row(
                table,
                str(target_file_index), target_file.as_posix(),
                "=>", changed_target_file.as_posix(),
                str(rename_success)
            )

    print()


def clean_directory(
        directory: Union[Text, Path],
        recursive: bool = True
):
    """Cleans a directory from unwanted download artifacts - recursively by
    default."""

    target_directory: Path = normalize_directory(directory)

    remove_archive_file_lists(target_directory, recursive=recursive)
    rename_prefixed_files(target_directory, recursive=recursive)


COMMANDS = {
    "remove_filelist": remove_archive_file_lists,
    "rename_prefixed": rename_prefixed_files,
    "clean": clean_directory,
    "clean_cwd": lambda **kwargs: clean_directory(
        directory=Path.cwd(),
        **kwargs
    )
}

if __name__ == "__main__":
    try:
        import fire
        fire.Fire(COMMANDS)
    except ImportError:
        if input(
                "Type 'yes' to recursively clean "
                "current working directory from "
                "Stud.IP files and prefixes. > "
        ) == "yes":
            clean_directory(
                directory=Path.cwd(),
                recursive=True
            )
